# jonsi - Jonas' ANSI image creator

Yet another image to ANSI converter. This one can do three different modes, full block, half block and quadrant block.
For the two later modes, unicode is required. A true color terminal is also required.

There are other block elements among the Unicode blocks, but due to that you only can have two colors per character, I guess there is no big gain compared to use only full, half and quadrant blocks.

Example usage: `cmd/jonsiviewer/jonsiviewer.go`

## Examples
Original image                     | FullBlock                            | HalfBlock                             | QuaterBlock
-----------------------------------|--------------------------------------|---------------------------------------|---------------------------------------
![srcImage](examples/kodim23.png) | ![dstImage](examples/parrots0.jpg)  | ![dstImage](examples/parrots1.jpg)  | ![dstImage](examples/parrots2.jpg)
![srcImage](examples/girl.png) | ![dstImage](examples/girl0.jpg)  | ![dstImage](examples/girl1.jpg)  | ![dstImage](examples/girl2.jpg)


