module bitbucket.org/gmelchett/jonsi

go 1.16

require (
	github.com/disintegration/imaging v1.6.2
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
)
