package jonsi

import (
	"fmt"
	"image"
	"image/color"
	"io"
	"os"
	"strings"

	_ "golang.org/x/image/webp"

	"github.com/disintegration/imaging"
	"golang.org/x/crypto/ssh/terminal"
)

const (
	FullBlock = iota
	HalfBlock
	QuadrantBlock
	NumModes
)

const (
	FullWidth       = -1
	FullHeight      = -1
	KeepAspectRatio = 0
)

var blocks = map[int]string{
	0:    " ",      //      Space
	1:    "\u2597", // ▗ 	Quadrant lower right
	10:   "\u2596", // ▖ 	Quadrant lower left
	11:   "\u2584", // ▄ 	Lower half block
	100:  "\u259D", // ▝ 	Quadrant upper right
	101:  "\u2590", //▐ 	Right half block
	110:  "\u259E", // ▞ 	Quadrant upper right and lower left
	111:  "\u259F", // ▟ 	Quadrant upper right and lower left and lower right
	1000: "\u2598", // ▘ 	Quadrant upper left
	1001: "\u259A", // ▚ 	Quadrant upper left and lower right
	1010: "\u258C", // ▌ 	Left half block
	1011: "\u2599", // ▙ 	Quadrant upper left and lower left and lower right
	1100: "\u2580", // ▀ 	Upper half block
	1101: "\u259C", // ▜ 	Quadrant upper left and upper right and lower right
	1110: "\u259B", // ▛ 	Quadrant upper left and upper right and lower left
	1111: "\u2588", // █ 	Full block
}

type Block struct {
	Bg   color.NRGBA
	Fg   color.NRGBA
	Char string
}

func scale(origImg image.Image, rows, cols, rowMul, colMul int) (*image.NRGBA, int, int, error) {

	// default to 80x
	if rows == KeepAspectRatio && cols == KeepAspectRatio {
		cols = 80
	}

	if rows == FullHeight || cols == FullWidth {
		width, height, err := terminal.GetSize(int(os.Stdin.Fd()))
		if err != nil {
			return nil, 0, 0, err
		}
		if rows == FullHeight {
			rows = height
		}
		if cols == FullWidth {
			cols = width / 2
		}
	}

	// Since we don't have square pixels, aspect ratio must be calculated differently
	if rows == KeepAspectRatio || cols == KeepAspectRatio {
		bounds := origImg.Bounds()
		if rows == KeepAspectRatio {
			rows = int(float64(cols) * float64(bounds.Max.Y) / float64(bounds.Max.X))
		}
		if cols == KeepAspectRatio {
			cols = int(float64(rows) * float64(bounds.Max.X) / float64(bounds.Max.Y))
		}
	}

	img := imaging.Resize(origImg, cols*colMul, rows*rowMul, imaging.Lanczos)

	bounds := img.Bounds()

	return img, bounds.Max.X, bounds.Max.Y, nil
}

func setColor(ansiCode string, curr, prev *color.NRGBA) string {

	if prev == nil || prev.R != curr.R || prev.G != curr.G || prev.B != curr.B {
		return fmt.Sprintf("%s;%d;%d;%dm", ansiCode, curr.R, curr.G, curr.B)
	} else {
		return ""
	}
}

const FG = "\033[38;2"
const BG = "\033[48;2"

func blk2str(blkImg [][]*Block) string {

	var str strings.Builder

	for y := range blkImg {

		var oldBG, oldFG *color.NRGBA
		for x := range blkImg[y] {
			str.WriteString(setColor(BG, &blkImg[y][x].Bg, oldBG))
			str.WriteString(setColor(FG, &blkImg[y][x].Fg, oldFG))
			str.WriteString(blkImg[y][x].Char)
			oldBG = &blkImg[y][x].Bg
			oldFG = &blkImg[y][x].Fg
		}
		str.WriteString("\033[0m\n")
	}
	return str.String()
}

func renderHalfBlock(origImg image.Image, rows, cols int) ([][]*Block, int, int, error) {
	img, w, h, err := scale(origImg, rows, cols, 2, 2)
	if err != nil {
		return nil, -1, -1, err
	}

	blkImg := make([][]*Block, 0, h/2)

	for y := 0; y < h; y += 2 {
		blkLine := make([]*Block, 0, w)
		for x := 0; x < w; x++ {
			blkLine = append(blkLine, &Block{Bg: img.NRGBAAt(x, y), Fg: img.NRGBAAt(x, y+1), Char: "\u2584"})
		}
		blkImg = append(blkImg, blkLine)
	}
	return blkImg, h / 2, w / 2, nil
}

func renderFullBlock(origImg image.Image, rows, cols int) ([][]*Block, int, int, error) {

	img, w, h, err := scale(origImg, rows, cols, 1, 2)
	if err != nil {
		return nil, -1, -1, err
	}

	blkImg := make([][]*Block, 0, h)

	for y := 0; y < h; y++ {
		blkLine := make([]*Block, 0, w)

		for x := 0; x < w; x++ {
			blkLine = append(blkLine, &Block{Bg: img.NRGBAAt(x, y), Fg: color.NRGBA{}, Char: " "})
		}
		blkImg = append(blkImg, blkLine)
	}
	return blkImg, h, w / 2, nil
}

func abs(a, b uint32) uint32 {
	if a > b {
		return a - b
	} else {
		return b - a
	}
}

type rgb struct {
	r, g, b uint32
}

func deltaColor(a, b rgb) uint32 {
	return abs(a.r, b.r) + abs(a.g, b.g) + abs(a.b, b.b)
}

func renderQuadrantBlock(origImg image.Image, rows, cols int) ([][]*Block, int, int, error) {

	img, w, h, err := scale(origImg, rows, cols, 2, 4)
	if err != nil {
		return nil, -1, -1, err
	}

	blkImg := make([][]*Block, 0, h/2)

	for y := 0; y < h; y += 2 {

		blkLine := make([]*Block, 0, w/2)

		for x := 0; x < w; x += 2 {

			xd := [4]int{0, 1, 0, 1}
			yd := [4]int{0, 0, 1, 1}

			var colors [4]rgb

			for i := range colors {
				colors[i].r, colors[i].g, colors[i].b, _ = img.At(x+xd[i], y+yd[i]).RGBA()
			}

			var biggestDelta uint32
			var groupIdx [2]int

			for i := range colors {
				for j := range colors {
					if j == i {
						continue
					}
					d := deltaColor(colors[i], colors[j])
					if biggestDelta < d {
						biggestDelta = d
						groupIdx[0] = i
						groupIdx[1] = j
					}
				}
			}

			var pattern int

			colorOut := [2]rgb{colors[groupIdx[0]], colors[groupIdx[1]]}
			numColors := [2]uint32{1, 1}
			patternVal := [4]int{1000, 100, 10, 1}

			for i := range colors {
				if i == groupIdx[1] {
					pattern += patternVal[i]
				} else if i != groupIdx[0] {
					d0 := deltaColor(colors[i], colors[groupIdx[0]])
					d1 := deltaColor(colors[i], colors[groupIdx[1]])
					idx := 0
					if d0 > d1 {
						idx++
						pattern += patternVal[i]
					}
					colorOut[idx].r += colors[i].r
					colorOut[idx].g += colors[i].g
					colorOut[idx].b += colors[i].b

					numColors[idx]++
				}
			}

			for i := range numColors {
				colorOut[i].r /= numColors[i]
				colorOut[i].g /= numColors[i]
				colorOut[i].b /= numColors[i]
			}

			blkLine = append(blkLine, &Block{
				Bg:   color.NRGBA{R: uint8(colorOut[0].r / 256), G: uint8(colorOut[0].g / 256), B: uint8(colorOut[0].b / 256)},
				Fg:   color.NRGBA{R: uint8(colorOut[1].r / 256), G: uint8(colorOut[1].g / 256), B: uint8(colorOut[1].b / 256)},
				Char: blocks[pattern]})

		}
		blkImg = append(blkImg, blkLine)
	}
	return blkImg, h / 2, w / 4, nil
}

func NewBlockFromImage(img image.Image, rows, cols int, mode int) ([][]*Block, int, int, error) {
	switch mode {
	case FullBlock:
		return renderFullBlock(img, rows, cols)
	case HalfBlock:
		return renderHalfBlock(img, rows, cols)
	case QuadrantBlock:
		return renderQuadrantBlock(img, rows, cols)
	default:
		return nil, -1, -1, fmt.Errorf("jonsi: Unknown mode: %d", mode)
	}
}

func NewBlockFromReader(reader io.Reader, rows, cols int, mode int) ([][]*Block, int, int, error) {
	if img, _, err := image.Decode(reader); err == nil {
		return NewBlockFromImage(img, rows, cols, mode)
	} else {
		return nil, 0, 0, err
	}
}

func NewFromReader(reader io.Reader, rows, cols int, mode int) (string, int, int, error) {
	if blk, h, w, err := NewBlockFromReader(reader, rows, cols, mode); err == nil {
		return blk2str(blk), h, w, nil
	} else {
		return "", -1, -1, err
	}
}

func NewFromImage(img image.Image, rows, cols int, mode int) (string, int, int, error) {
	if blk, h, w, err := NewBlockFromImage(img, rows, cols, mode); err == nil {
		return blk2str(blk), h, w, nil
	} else {
		return "", -1, -1, err
	}
}

func NewFromFile(fileName string, rows, cols int, mode int) (string, int, int, error) {
	reader, err := os.Open(fileName)
	if err != nil {
		return "", -1, -1, err
	}
	defer reader.Close()
	return NewFromReader(reader, rows, cols, mode)
}
