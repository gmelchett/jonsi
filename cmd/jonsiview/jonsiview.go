package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"

	"bitbucket.org/gmelchett/jonsi"
)

func main() {

	var mode, rows, cols int
	var clearScreen bool

	flag.IntVar(&mode, "m", jonsi.QuadrantBlock, "Render mode: 0 = Full blocks, 1 = Half blocks, 2 = Quadrant blocks.")
	flag.IntVar(&rows, "r", jonsi.KeepAspectRatio, "Render width: -1 = full terminal width, 0 = keep aspect ratio, > 0 size in characters.")
	flag.IntVar(&cols, "c", jonsi.KeepAspectRatio, "Render height: -1 = full terminal height, 0 = keep aspect ratio, > 0 size in characters.")
	flag.BoolVar(&clearScreen, "s", false, "Don't clear screen before output.")

	flag.Parse()

	images := flag.Args()

	if len(images) == 0 {
		fmt.Printf("syntax: %s <flags> image file(s)..\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	r := bufio.NewReader(os.Stdin)

	for i := range images {
		if !clearScreen {
			fmt.Print("\033[H\033[2J")
		}

		if s, _, _, err := jonsi.NewFromFile(images[i], rows, cols, mode); err == nil {
			fmt.Println(s)
		} else {
			fmt.Println(err)
			os.Exit(1)
		}
		if (i + 1) < len(images) {
			fmt.Print("Press enter for next image:")
			r.ReadByte()
		}
	}
}
